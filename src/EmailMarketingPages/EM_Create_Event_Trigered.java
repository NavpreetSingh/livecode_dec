package EmailMarketingPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EM_Create_Event_Trigered {

	public WebDriver driver;
	
	By Event_name_edit  = By.xpath("//*[@id = 'transactional_email_name_span']");
	
	By Check_Stats = By.xpath("//*[text() = 'Check Stats']");
	
	By 	Event_Name = By.xpath("//*[@id = 'eventTypeId']");
	
	By Choose_Delay = By.xpath("//*[@id = 'delayMsTime']");
	
	By Delay_Seconds = By.xpath("//*[@id = 'delayMsId']']");
	
	By Delay_Minutes = By.xpath("//*[@id = 'delayMsId']/option[2]"); 	
	
	By Delay_Hours = By.xpath("//*[@id = 'delayMsId']/option[3]");

	By Delay_Days = By.xpath("//*[@id = 'delayMsId']/option[4]");
	
	By Choose_Lifecycle = By.xpath("//*[@id = 'lifeCycleIds']");
	
	By Choose_Tags = By.xpath("(//*[@title= 'None selected'])[1]");
		
	By Choose_Segments = By.xpath("(//*[@title= 'None selected'])[2]");
	
	By Check_time_interval = By.xpath("//*[@class= 'mobile_label']");
	
	By Add_email_button = By.xpath("//*[@id= 'add_email_div']");
	
	
	 public EM_Create_Event_Trigered (WebDriver driver)
		{

			this.driver = driver;
		}
	
	 public void Event_name_edit(String Edit_Event_name){
			
			driver.findElement(Event_name_edit).sendKeys(Edit_Event_name);
			
			}
	 public void Check_Stats(){
			
			driver.findElement(Check_Stats).click();
			
			}
	 public void Event_Name(){
			
			driver.findElement(Event_Name).click();// Need to use select as it is a drop down
			
			}
	 public void Choose_Delay(String Delay_time){
			
			driver.findElement(Choose_Delay).sendKeys(Delay_time);
			
			}
	 public void Delay_Seconds(){
			
			driver.findElement(Delay_Seconds).click();
			
			}
	 public void Delay_Minutes() throws Exception {
			
		    driver.findElement(Delay_Seconds).click();
		   
		    Thread.sleep(1000);
			
		    driver.findElement(Delay_Minutes).click();
			
			
			}
	 public void Delay_Hours() throws Exception {
		 
		    driver.findElement(Delay_Seconds).click();
		    
		    Thread.sleep(1000);
			
		    driver.findElement(Delay_Hours).click();
			
			}
	 public void Delay_Days () throws Exception {
		 
		    driver.findElement(Delay_Seconds).click();
		    
		    Thread.sleep(1000);
		    
			driver.findElement(Delay_Days).click();
			
			}
	 public void Choose_Lifecycle(){
			
			driver.findElement(Choose_Lifecycle).click(); // Need to use select as it is a drop down
			
			}
	 public void Choose_Tags(){
			
			driver.findElement(Choose_Tags).click();
			
			}
	 public void Choose_Segments(){
			
			driver.findElement(Choose_Segments).click();
			
			}
	 public void Check_time_interval(){
			
			driver.findElement(Check_time_interval).click();
			
			}
	 public void Add_email_button(){
			
			driver.findElement(Add_email_button).click();
			
			}
	
	 
	 
	 
	
	
}
