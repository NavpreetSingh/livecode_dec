package SMSMarketingScenarios;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import SMSMarketingPages.SMSMarketing_Broadcast_Active;
import SMSMarketingPages.SMSMarketing_CampaignType;
import SMSMarketingPages.SMSMarketing_Design;
import SMSMarketingPages.SMSMarketing_Homepage;
import SMSMarketingPages.SMSMarketing_Target;
import common_Classes.*;
import promocode_Pages.Onsite_CreateCampaign;
import promocode_Pages.Onsite_Select_Template;
	
public class SMSMarketing_BroadcastCampaignCreation{
	
	// make changes on line number - 64, 83

	public static WebDriver driver;

	@Test
	public void SMS_BroadcastCampaignCreate() throws Exception
	
	{
		System.out.println("Scenario : SMS Marketing > Campaigns > Broadcast > Create Campaign > Print counts and stats > Export CSV");
		
		Setup_class set = new Setup_class(driver);
		Properties prop = set.loadPropertyFile();
		
		driver = set.setup();

		set.getLiveURL();
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
		obj.uname(set.getWoocommercetestUsername());
		obj.upswd(set.getWoocommercetestPassword());
		obj.ulogin();
		
		/*// Add code to select account
		set.selectAccountNav();*/
		
		System.out.println("User has logged in successfully");
		
		Homepage home_obj = new Homepage(driver);
		home_obj.sms_mktg_click();
		Thread.sleep(3000);
		
		SMSMarketing_Homepage page_obj = new SMSMarketing_Homepage(driver);
		page_obj.Campaigns_BroadCast_CreateCampaignClick();
		
		SMSMarketing_CampaignType type_obj = new SMSMarketing_CampaignType(driver);
		type_obj.CampaignTypeClick();
	//	Date date1 = set.getCurrentDateTime();
		String name = "Auto_broadcast"+" "+System.currentTimeMillis();
		type_obj.CampaignNameClick(name);
		type_obj.PrintChooseCampaignHeading();
		type_obj.PrintCampaignNameHeading();
		type_obj.SaveNextClick();
		Thread.sleep(5000);
		
		SMSMarketing_Design design_obj = new SMSMarketing_Design(driver);
		design_obj.ChooseGatewayClick();
		design_obj.SMSTextClick("This is test message for sms marketing");
		design_obj.SaveNextClick();
		Thread.sleep(5000);
		
		SMSMarketing_Target target_obj = new SMSMarketing_Target(driver);
		
		target_obj.ANDConditionClick();
		target_obj.ANDDropdown1Click();
		target_obj.ANDDropdown2Click();
		target_obj.ANDDropdown3Click();
		target_obj.ANDDropdown4Click(prop.getProperty("SMSNumber"));
		target_obj.RefreshContactsClick();
		Thread.sleep(8000);
		design_obj.SaveNextClick();
		Thread.sleep(5000);
		
		SMSMarketing_Broadcast_Active active_obj = new SMSMarketing_Broadcast_Active(driver);
		active_obj.SendNowClick();
		Thread.sleep(9000);
		
		driver.navigate().refresh();

		// Assert name of campaign
		
		String BroadcastCampName = page_obj.printCampaignName();
		System.out.println("Campaign Name = " +BroadcastCampName );
		
		SoftAssert s_assert = new SoftAssert();
		s_assert.assertEquals(BroadcastCampName, name);
		s_assert.assertAll();
		System.out.println("Assertion -> Name of the campaign is matched");
	
		// Code to take out StatsURL and write on a file
		
		String campID = driver.findElement(page_obj.CampaignName).getAttribute("campatypid");
		System.out.println(campID);         
		
		String URL = driver.findElement(By.xpath("//*[contains(@href,'/sms/campaign-stats/nv/cType/nl/cTypeId/" +campID+ "')]")).getAttribute("href");
		System.out.println(URL); // write this value on file and use it in analytics
		
		String filename = set.getSMSBroadcastFileName();
		
		set.BufferWriterMethod(filename, URL);
		System.out.println("Data written in the file = " + URL);
			
		// Move to Analytics scenario
		
		
	}
	
	@AfterMethod
	public void kill()
	{
		System.out.println("Closing the browser now");
		driver.quit();
		
		
	}

	
}