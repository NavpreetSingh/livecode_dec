package SMSMarketingScenarios;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import SMSMarketingPages.SMSMarketing_Broadcast_Active;
import SMSMarketingPages.SMSMarketing_CampaignType;
import SMSMarketingPages.SMSMarketing_Design;
import SMSMarketingPages.SMSMarketing_Homepage;
import SMSMarketingPages.SMSMarketing_Recurring_Active;
import SMSMarketingPages.SMSMarketing_Target;
import common_Classes.*;
import promocode_Pages.Onsite_CreateCampaign;
import promocode_Pages.Onsite_Select_Template;

public class SMSMarketing_RecurringWeeklyAnalytics {
	
	// make changes on line number - 63, 82

	public static WebDriver driver;

	@Test
	public void SMS_BroadcastCampaignDailyAnalyze() throws Exception
	
	{
		System.out.println("Scenario : SMS Marketing > Campaigns > Broadcast > Create Campaign > Print counts and stats > Export CSV");
		
		Setup_class set = new Setup_class(driver);
		Properties prop = set.loadPropertyFile();
		
		driver = set.setup();

		set.getLiveURL();
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
		obj.uname(set.getWoocommercetestUsername());
		obj.upswd(set.getWoocommercetestPassword());
		obj.ulogin();
		
		/*// Add code to select account
		set.selectAccountNav();*/
		
		System.out.println("User has logged in successfully");
		
		String filename = set.getSMSRecurWeeklyFileName();
		String URL = set.BufferReaderMethod(filename);
		System.out.println("Data present in file = " + URL);
		
		driver.get(URL);           //Stats page
		
		// Add code for Counts, Stats
	
		SoftAssert s_assert = new SoftAssert();
		SMSMarketing_Homepage page_obj = new SMSMarketing_Homepage(driver);
		
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile,
				new File("./Screenshots/" + this.getClass() + System.currentTimeMillis() + ".png"));

		System.out.println("Screenshot of Stats page is taken successfully");

		boolean value = page_obj.SMS_PrintContactsFromStats(prop.getProperty("SMSNumber"));
		System.out.println("Boolean value for Stats = " + value);

		if (value == true) {
			System.out.println("PASS , i.e. Required Phone Number is contained in the Sent heading.");
		} else {
			System.out.println("FAIL , i.e. Required Phone Number is NOT contained in Sent heading");
		}

		s_assert.assertEquals(value, true);
		
		// Click on Export CSV from Sent heading
		driver.switchTo().frame(driver.findElement(page_obj.SentIframe));
		
		page_obj.ExportCSVclick();
		Thread.sleep(3000);
		
		// Move to new tab and click on download icon, capture screenshot
		
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1)); // switches to new tab
		
		page_obj.DownloadCSVclick();
		
		Thread.sleep(3000);
		
		File scrFile1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile1,
				new File("./Screenshots/" + this.getClass() + System.currentTimeMillis() + ".png"));
		
		System.out.println("Screenshot is taken after clicking on download icon");
		
		s_assert.assertAll();
		
	}

	@AfterMethod
	public void kill()
	{
		System.out.println("Closing the browser now");
		driver.quit();
		
		
	}

}
	

