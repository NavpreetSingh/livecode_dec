package SMSMarketingScenarios;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import SMSMarketingPages.SMSMarketing_Broadcast_Active;
import SMSMarketingPages.SMSMarketing_CampaignType;
import SMSMarketingPages.SMSMarketing_Design;
import SMSMarketingPages.SMSMarketing_Homepage;
import SMSMarketingPages.SMSMarketing_Recurring_Active;
import SMSMarketingPages.SMSMarketing_Target;
import common_Classes.*;
import promocode_Pages.Onsite_CreateCampaign;
import promocode_Pages.Onsite_Select_Template;

public class SMSMarketing_RecurringDailyCreation {


	public static WebDriver driver;

	@Test
	public void SMS_RecurringDailyCreate() throws Exception
	
	{
		System.out.println("Scenario : SMS Marketing > Campaigns > Recurring> Daily > Create Campaign> Print counts and stats > Export CSV");
		
		Setup_class set = new Setup_class(driver);
		Properties prop = set.loadPropertyFile();
		
		driver = set.setup();
	
		set.getLiveURL();
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
		obj.uname(set.getWoocommercetestUsername());
		obj.upswd(set.getWoocommercetestPassword());
		obj.ulogin();
		
		/*// Add code to select account
		set.selectAccountNav();*/
		
		System.out.println("User has logged in successfully");
		
		Homepage home_obj = new Homepage(driver);
		home_obj.sms_mktg_click();
		Thread.sleep(3000);
		
		SMSMarketing_Homepage page_obj = new SMSMarketing_Homepage(driver);
		page_obj.Campaigns_RecurringClick();
		Thread.sleep(5000);	
		page_obj.Campaigns_Recurring_CreateCampaignClick();
		
		
		SMSMarketing_CampaignType type_obj = new SMSMarketing_CampaignType(driver);
		type_obj.CampaignTypeClick();
		String name = "Auto_rec_daily"+" "+System.currentTimeMillis();
		type_obj.CampaignNameClick(name);
		type_obj.PrintChooseCampaignHeading();
		type_obj.PrintCampaignNameHeading();
		type_obj.SaveNextClick();
		Thread.sleep(5000);
		
		SMSMarketing_Design design_obj = new SMSMarketing_Design(driver);
		design_obj.ChooseGatewayClick();
		design_obj.SMSTextClick("This is test message for Recurring SMS Marketing -> Daily");
		design_obj.SaveNextClick();
		Thread.sleep(5000);
		
		SMSMarketing_Target target_obj = new SMSMarketing_Target(driver);
		
		target_obj.ANDConditionClick();
		target_obj.ANDDropdown1Click();
		target_obj.ANDDropdown2Click();
		target_obj.ANDDropdown3Click();
		target_obj.ANDDropdown4Click(prop.getProperty("SMSNumber"));
		target_obj.RefreshContactsClick();
		Thread.sleep(10000);
		design_obj.SaveNextClick();
		Thread.sleep(5000);
	
		SMSMarketing_Recurring_Active rec_obj = new SMSMarketing_Recurring_Active(driver);
		rec_obj.DailyClick();
		
		rec_obj.Daily_StartDateClick();
		rec_obj.Daily_EndDateClick();
		rec_obj.EndDate_NextMonthClick();
		rec_obj.EndDate_SelectDateClick();
		rec_obj.Daily_TimeClick();
	
		set.setScheduleDateAndTime();   // To calculate -5:30hrs
		
		Thread.sleep(2000);
		
		rec_obj.Daily_ExclusionDateClick();
		Thread.sleep(2000);
		rec_obj.Daily_ExclusionDate_SelectDateClick();
		rec_obj.Exclusion_NextMonthClick();
		rec_obj.Exclusion_SelectDateClick();
		rec_obj.Daily_ExclusionDate_SaveClick();
		Thread.sleep(2000);
		rec_obj.Daily_PreviewClick();
		rec_obj.Daily_ExcludingPreviewClick();
		rec_obj.Daily_ScheduleClick();
		Thread.sleep(5000);
		

		// Assert name of campaign
	
		String DailyCampName = page_obj.RecurringCampaignName();
		System.out.println("Campaign Name = " +DailyCampName);
		
		SoftAssert s_assert = new SoftAssert();
		s_assert.assertEquals(DailyCampName, name);
		s_assert.assertAll();
		System.out.println("Assertion -> Name of the campaign is matched");
		
		String campaigndetails = page_obj.RecurringCampaignDetails();
		System.out.println("Start Date, End date and Time of Campaign = "+campaigndetails);
	
		Thread.sleep(150000);    // Wait for 2.5 min for recurring campaign to appear on Broadcast page
		
		page_obj.Campaigns_BroadCastClick();            // Move to broadcast page
		
		driver.navigate().refresh();
		
		// Add code for writing StatsURL in file
		
		String campID = driver.findElement(page_obj.CampaignName).getAttribute("campatypid");
		System.out.println(campID);         
		
		String URL = driver.findElement(By.xpath("//*[contains(@href,'/sms/campaign-stats/nv/cType/nl/cTypeId/" +campID+ "')]")).getAttribute("href");
		System.out.println(URL); // write this value on file and use it in analytics
		
		String filename = set.getSMSRecurDailyFileName();
		
		set.BufferWriterMethod(filename, URL);
		System.out.println("Data written in the file = " + URL);
		
		
		
		
		// Move to Analytics scenario
	
	}

	@AfterMethod
	public void kill()
	{
		System.out.println("Closing the browser now");
		driver.quit();
		
		
	}

}
	

