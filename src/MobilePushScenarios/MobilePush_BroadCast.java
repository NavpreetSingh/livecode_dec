package MobilePushScenarios;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import MobilePushPages.MobilePush_SendOrSchedulePage;
import MobilePushPages.createCampaignPage;
import MobilePushPages.mobilePush_DesignPage;
import MobilePushPages.mobilePush_TargetingPage;
import common_Classes.Account_Settings;
import common_Classes.Homepage;
import common_Classes.Loginpage;
import common_Classes.Setup_class;

public class MobilePush_BroadCast {

	public static WebDriver driver;
	
	
	@Test
	
	public void CreateBroadCastCampaign() throws Exception
	
	{
		Setup_class set = new Setup_class(driver);
		
		driver = set.setup();
	
	//	driver.get("https://pre-prod-102.betaout.com/");
		set.getPreProdURL();	
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
	//	obj.uname("betaoutpush.multiple@getamplify.com");
		obj.uname(set.getBetaoutpushUsername());
	//	obj.upswd("123456");
		obj.upswd(set.getBetaoutpushPassword());
		obj.ulogin();
		
		System.out.println("User has logged in successfully");
		
		Homepage homeobj = new Homepage(driver);
		homeobj.mobilepush_click();
		
		createCampaignPage obj1 = new createCampaignPage(driver);
		obj1.campaignButton();
		String campaignNameEntered = "broadcamp"+System.currentTimeMillis();
		obj1.broadcastImage();
		obj1.enterCampaignName(campaignNameEntered);
		obj1.clickOnSaveandNextButton();
		
		Thread.sleep(5000);
		
		mobilePush_DesignPage obj2 = new mobilePush_DesignPage(driver);
		
		String titleEntered = "Title"+System.currentTimeMillis();
		obj2.setMobilePushTitle(titleEntered);
		
		String messageEntered = "Message"+System.currentTimeMillis();
		obj2.setMobilePushMessageBody(messageEntered);
		obj2.clickOnSaveAndNextButton();
		
		Thread.sleep(5000);
		
		mobilePush_TargetingPage obj3 = new mobilePush_TargetingPage(driver);
		
		obj3.clickOnAddButton();
		obj3.ANDDropdown1Click();
		obj3.ANDDropdown2Click();
		obj3.ANDDropdown3Click();
		obj3.ANDDropdown4Click("eCgM_hU7guc:APA91bHcLJYwPJi-EXNdGnETjH3ShFNfzgCNuAZyrlhx6jVwVa4B0pTShXOE5paaIjuQW4Fw0X9y65SqAqhKYgZQpx2VpmcnVoDodhuAwx8NKizKn6GDJwLNUFqcrSHP55Cez42_MbQF");   // GCM id 
		
		obj3.clickOnRefreshButton();
		Thread.sleep(5000);
		
		obj2.clickOnSaveAndNextButton();
		Thread.sleep(5000);
		
		MobilePush_SendOrSchedulePage obj4 = new MobilePush_SendOrSchedulePage(driver);
		obj4.clickOnSendNowButtonInPage();
		Thread.sleep(2000);
		obj4.clickOnSendNowButtonInPopUp();
		Thread.sleep(10000);
		
		String camaignNameFetched = obj1.CampaignNamePrint();
		System.out.println("Fetched campaign name = "+camaignNameFetched);

		obj1.ConditionsOfCampaignClick();
		Thread.sleep(2000);
		
		/*obj1.DeleteCampaignClick();
		Thread.sleep(2000);*/
		
		
		SoftAssert sa = new SoftAssert();
		sa.assertEquals(campaignNameEntered, camaignNameFetched);
		sa.assertAll();
		System.out.println("Assertion of campaign name is done");
		
		
		// Open Contact DB > Properties to verify data of 'pn_last_sent_date' field
		
		
		homeobj.contact_db_click();
		
		driver.findElement(By.xpath("//*[@id = 'search-trigger']")).click();     // Search box		
	//	driver.findElement(By.xpath("(//*[@src = '/parmanu2/images/header/search.png'])[2]")); // Search box with new headers
		
		driver.findElement(By.xpath("(//*[@name = 'search'])[3]")).sendKeys("latest123");   // Enter in Search box
	//  driver.findElement(By.xpath("(//*[@name = 'search'])[1]")).sendKeys("latest123");   // Enter in Search box with new headers	
	
		
		driver.findElement(By.xpath("(//*[@class = 'boicon-search'])[3]")).click();   // Click on Search
	//	driver.findElement(By.xpath("(//*[@name = 'searchSubmit'])[1]")).click();  // Click on Search with new headers
		Thread.sleep(5000);
		
		driver.findElement(By.xpath("(//*[contains(@href,'/users/user-permalink/nv/userId')])[1]")).click();  // Open Contact
		Thread.sleep(5000);
		
		// Add timer for 2 hours
	//	Thread.sleep((long) 7.2e+6);
	//	Thread.sleep(7200000);
		Thread.sleep(10000);
		
		driver.findElement(By.xpath("(//*[text() = 'Properties'])[2]")).click(); // Properties
		Thread.sleep(5000);
 		
		String pn_lastsentdate = driver.findElement(By.xpath("(//*[text()='pn_last_sent_date']/../td)[2]")).getText();
		System.out.println("pn_last_sent_date = "+pn_lastsentdate);
		
		driver.quit();
		
	}
	
}
