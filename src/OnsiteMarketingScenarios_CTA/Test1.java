package OnsiteMarketingScenarios_CTA;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import EmailMarketingPages.EM_Broadcast_Template;
import common_Classes.*;
import OnsiteMarketingPages.Onsite_CreateCampaign;
import OnsiteMarketingPages.Onsite_Design;
import OnsiteMarketingPages.Onsite_Select_Template;
import OnsiteMarketingPages.Onsite_Target_Page;

public class Test1 {

	public static WebDriver driver;
	
	// Before running scenario, make changes on line number - 61
	
	
	@Test
	public void CallToActionBanner() throws Exception
	
	{
		System.out.println("Scenario : Onsite Marketing > Select CTABannerTemplate > Design > Target > Active");
		
	
		Setup_class set = new Setup_class(driver);
			
		driver = set.setup();
		
		set.getLiveURL();
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
		obj.uname(set.getWoocommercetestUsername());
		obj.upswd(set.getWoocommercetestPassword());
		obj.ulogin();
		
		System.out.println("User has logged in successfully");
	
		
		
/*		// Correct code for cart recovery
		driver.get("https://app.betaout.com/carts/cart-campaign");
	
		
		String CampID = driver.findElement(By.xpath("//*[@style = ' display: inline-block;']")).getAttribute("campatypid");
		System.out.println(CampID);
		
		String il = driver.findElement(By.xpath("//*[@id = '"+CampID+"_totalSent']")).getText();
		System.out.println(il);*/
		
/*		driver.get("https://stag-f1.betaout.in/carts/cart-campaign");
		
		String CampID = driver.findElement(By.xpath("//*[@class = 'cart-camp-name']")).getAttribute("campatypid");
		System.out.println(CampID);
		
		String il = driver.findElement(By.xpath("//*[@id = '"+CampID+"_totalSent']")).getText();
		System.out.println(il);*/
		
/*		String a = "qa.betaout.in";
		String b = "/sms/cart";

		String combo = a+b;
		System.out.println(combo);
		
		Date date = new Date();
		SimpleDateFormat modified = new SimpleDateFormat("dd/MM/yyyy");
		String newDate = modified.format(date);
		System.out.println(newDate);
		
		long ab = System.currentTimeMillis()%1000;
		System.out.println(ab);
*/

		
		driver.get("https://app.betaout.com/email/create-new-newsletter/nv/cTypeId/36265/re/0/templateId/389");
		
		driver.findElement(By.xpath("//*[text() =' {{can_receive_email}}']")).click();
		driver.findElement(By.xpath("//*[text() =' {{can_receive_email}}']")).clear();
		
		
	}
	
}