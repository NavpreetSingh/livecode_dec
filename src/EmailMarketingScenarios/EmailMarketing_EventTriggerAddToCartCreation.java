package EmailMarketingScenarios;

import java.io.File;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import CartRecoveryPages.CartRecovery_Homepage;
import EmailMarketingPages.EM_Broadcast_Template;
import EmailMarketingPages.EM_Campains_Broadcast;
import EmailMarketingPages.EM_EventTriggeredCreateCampaign;
import GmailLogin.GmailLoginPages;
import common_Classes.Homepage;
import common_Classes.Loginpage;
import common_Classes.Setup_class;
/**
 * @author - Navpreet
 */
public class EmailMarketing_EventTriggerAddToCartCreation {
	
public static WebDriver driver;
	
	
	@Test
	public void EventTrigger_AddCartCreate() throws Exception{
		
		Setup_class set = new Setup_class(driver);
		
		driver = set.setup();
				
		// Code to load the Property file
		Properties prop = set.loadPropertyFile();
		
		set.getLiveURL();
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
		obj.uname(set.getWoocommercetestUsername());
		obj.upswd(set.getWoocommercetestPassword());
		
		obj.ulogin();
		
		/*// Add code to select account
		set.selectAccountNav();*/
		
		System.out.println("User has logged in successfully");
				
		Homepage homeobj = new Homepage(driver);
		homeobj.email_mktg_click();
		
		EM_Campains_Broadcast obj1 = new EM_Campains_Broadcast(driver);
		obj1.campaign_Tab();
		Thread.sleep(2000);
		obj1.EventTriggeredClick();
		Thread.sleep(2000);

		EM_EventTriggeredCreateCampaign eventobj = new EM_EventTriggeredCreateCampaign(driver);
		
		eventobj.EventTrigger_CreateCampaignButton();
		Thread.sleep(5000);
		String name = prop.getProperty("EventTriggerAddToCart_Name") + System.currentTimeMillis();
		
		eventobj.EventTrigger_CampaignName(name);
		
		eventobj.EventTrigger_SaveCampaign();
		Thread.sleep(5000);
		
		eventobj.SelectEventFromDropdown("added_to_cart");       // Add to cart event
		
		eventobj.Add_email_button();
		Thread.sleep(5000);
		
		eventobj.Template();
		Thread.sleep(8000);
		
		String subject_data = prop.getProperty("Email_EventTriggerAddToCart_Subject") + System.currentTimeMillis();
		
		eventobj.Campaign_Subject(subject_data);
		
	//	eventobj.Sender_Id(prop.getProperty("EventTrigger_SenderID"));
		
		EM_Broadcast_Template obj2 = new EM_Broadcast_Template(driver);
		obj2.Sender_Id(prop.getProperty("SES_SenderID"));  
		Thread.sleep(2000);
		
		obj2.click_can_receive_email();
		Thread.sleep(1000);
		obj2.clear_can_receive_email();
		Thread.sleep(1000);
		
		eventobj.Save_and_Exit();
		Thread.sleep(3500);
		
		eventobj.Linkurl_TickCheckbox();
		Thread.sleep(1000);
		eventobj.Linkurl_NewURL(prop.getProperty("EventTriggerAddToCartNewURL"));
		Thread.sleep(2000);
		eventobj.Linkurl_PopUp_OK();
		Thread.sleep(3000);
		
		eventobj.EventTrigger_MakeActive();
		Thread.sleep(2000);
		eventobj.EventTrigger_Save();
		
		
		// Print details of event
		
		String triggerName = eventobj.PrintEventTriggerName();
		String triggerEventName = eventobj.PrintEventTrigger_TriggerEvent();
		String triggerLifecycleName = eventobj.PrintEventTrigger_LifecycleStage();
		
		System.out.println("Trigger Name = "+triggerName +" "+"Event Name = "+triggerEventName +" "+"Lifecycle Name = "+triggerLifecycleName);
		
		
		// Now open new tab, login and add a product to cart
		
		Thread.sleep(5000);
		
		((JavascriptExecutor) driver).executeScript("window.open('','_blank');");

		Thread.sleep(2000);
		
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		 
		driver.switchTo().window(tabs.get(1)); //switches to first tab
		
		set.getWoocommerceURL();
		
		CartRecovery_Homepage carthome_obj = new CartRecovery_Homepage(driver);
		
		driver.findElement(carthome_obj.WoocommerceAccount_MyAccount).click();  // My Account
		
		Thread.sleep(2000);
		driver.findElement(carthome_obj.WoocommerceAccount_Username).sendKeys(prop.getProperty("GmailWoocommerceOneUsername"));
		driver.findElement(carthome_obj.WoocommerceAccount_Password).sendKeys(prop.getProperty("GmailWoocommerceOnePassword"));
		
		driver.findElement(carthome_obj.WoocommerceAccount_Login).click();
		Thread.sleep(3000);
		
		driver.findElement(carthome_obj.WoocommerceAccount_Home).click();  // Home
		
		driver.findElement(carthome_obj.WoocommerceAccount_AddToCart).click();  // Add to cart
		
		Thread.sleep(150000); // Wait for 2.5 min
		
		// Open Gmail to verify email
	
		set.getGmailURL();

		Thread.sleep(5000); 		
		
		driver.findElement(By.xpath(prop.getProperty("GmailSignInClick"))).click();
		
		GmailLoginPages page_obj = new GmailLoginPages(driver);
		
		page_obj.enterUsername(prop.getProperty("GmailQAValidUsername"));   
		page_obj.nextButtonClick();
		
		Thread.sleep(2000);
		
		page_obj.enterPassword(prop.getProperty("GmailQAValidPassword"));
		page_obj.signInClick();
			
		Thread.sleep(7000);
		
		page_obj.openEmail();
	
		SoftAssert s_assert = new SoftAssert();
		page_obj.showDetails();
		
		Thread.sleep(1000);
		String FROMName = page_obj.printFROMName();
		System.out.println("Name of the sender = "+FROMName);
		s_assert.assertEquals(FROMName, prop.getProperty("Email_FROMName"));
		System.out.println("Assertion of FROM Name is done");
				
		String FROMEmailAddress = page_obj.printFROMEmailAddress();
		System.out.println("EmailAddress of the sender = "+FROMEmailAddress);
		boolean val = prop.getProperty("SES_SenderID").contains(FROMEmailAddress);
				
		s_assert.assertEquals(val, true);        
		System.out.println("Assertion of FROM Email Address is done"); 
		
		
		
		String emailsubject = page_obj.printemailSubject();
		System.out.println("Subject of email = "+emailsubject);
		String emailmessage = page_obj.printemailMessage();
		System.out.println("Message content of email = "+emailmessage);

		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("./Screenshots/"+this.getClass()+System.currentTimeMillis()+".png"));		
		System.out.println("Screenshot of email is taken successfully");
		
		s_assert.assertEquals(emailsubject,subject_data);
		
		System.out.println("Subject of the email is verified ");
		
		// Click on CTA and compare Short URL
	
		//	EM_Broadcast_Template obj2 = new EM_Broadcast_Template(driver);
		
		obj2.CTAButtonClick();
		Thread.sleep(7000);
	
		ArrayList<String> tabs1 = new ArrayList<String> (driver.getWindowHandles());
		
		System.out.println("Number of tabs1 = "+tabs1.size());
		Thread.sleep(3000);
		
		driver.switchTo().window(tabs1.get(2)); // go to new tab

		String ShortURL = driver.getCurrentUrl();
		System.out.println("URL opened on click of CTA Button = "+ShortURL);
		
		s_assert.assertEquals(prop.getProperty("EventTriggerAddToCartNewURL"), ShortURL);
		Thread.sleep(2000);
		
		// Go back to tab1 and delete email
		
		driver.switchTo().window(tabs.get(1));  // go to gmail and delete mail 
		
		Thread.sleep(2000);
		page_obj.DeleteAllMails();
		
		System.out.println("Email is deleted successfully");
	
		page_obj.accountClick();
		Thread.sleep(1500);
		page_obj.signout();
		
		// Get campaign id and write on file
		
		driver.get(prop.getProperty("EventTrigger_CampaignsURL"));
		

		// Code to get Campaign ID
		
		EM_Campains_Broadcast ob = new EM_Campains_Broadcast(driver);
		String fullid = driver.findElement(ob.sent_campaigns).getAttribute("id");
		System.out.println(fullid);
			
		String id = fullid.split("_")[1];
		System.out.println("ID of campaign  = " +id );
		
		String filename = set.getEventAddCartFileName();
		
		set.BufferWriterMethod(filename, id);
		System.out.println("Data written in the file = " + id);
		
		Thread.sleep(3000);
		
		s_assert.assertAll();
	}
	

	@AfterMethod
	public void kill() throws Exception
	{
		System.out.println("Closing the browser now");
		driver.quit();
		
	}

}
