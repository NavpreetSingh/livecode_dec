package EmailMarketingScenarios;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import EmailMarketingPages.EM_Broadcast_Template;
import EmailMarketingPages.EM_Campains_Broadcast;
import EmailMarketingPages.EM_EventTriggeredCreateCampaign;
import EmailMarketingPages.EM_Tageting_Page;
import EmailMarketingPages.EmailMarketing_RecurringActive;
import GmailLogin.GmailLoginPages;
import common_Classes.Homepage;
import common_Classes.Loginpage;
import common_Classes.Setup_class;

public class EmailMarketing_Recurring_DailyCreation {

	public static WebDriver driver;

	@Test
	public void RecurringCampaign_Daily() throws Exception {

		System.out.println("Scenario - Make a recurring campaign > Print counts and verify Stats");

		Setup_class set = new Setup_class(driver);

		driver = set.setup();

		// Code to load the Property file

		Properties prop = set.loadPropertyFile();
		set.getLiveURL();

		Loginpage obj = new Loginpage(driver);

		obj.loginbutton();
		obj.uname(set.getWoocommercetestUsername());
		obj.upswd(set.getWoocommercetestPassword());
		obj.ulogin();
		
		/*// Add code to select account
		set.selectAccountNav();*/

		System.out.println("User has logged in successfully");

		Homepage homeobj = new Homepage(driver);
		homeobj.email_mktg_click();

		EM_Campains_Broadcast obj1 = new EM_Campains_Broadcast(driver);
		obj1.campaign_Tab();
		Thread.sleep(2000);
		obj1.RecurringClick();
		Thread.sleep(6000);

		obj1.Broadcast_Createcapmain();
		Thread.sleep(2000);
		
		String name = prop.getProperty("Email_RecurringDaily_Name") + System.currentTimeMillis();
		obj1.Broadcast_campaign_title(name);
		
		String filename = set.getRecurDailyFileName();
		
		set.BufferWriterMethod(filename, name);
		System.out.println("Data written in the file = " + name);
		
		Thread.sleep(5000);

		obj1.Template();
		Thread.sleep(15000);

		EM_Broadcast_Template obj2 = new EM_Broadcast_Template(driver);
		String subject_data = prop.getProperty("Email_RecurringDaily_Subject") + System.currentTimeMillis();
		obj2.Campaign_Subject(subject_data);

	//	obj2.Sender_Id(prop.getProperty("Email_SenderID"));

		obj2.Sender_Id(prop.getProperty("SES_SenderID"));
		Thread.sleep(2000);
		
		obj2.click_can_receive_email();
		Thread.sleep(1000);
		obj2.clear_can_receive_email();
		Thread.sleep(1000);
		
		obj2.Save_and_NextStep();
		Thread.sleep(2000);
		obj2.Linkurl_PopUp_OK();
		Thread.sleep(10000);

		EM_Tageting_Page obj3 = new EM_Tageting_Page(driver);
		String email = prop.getProperty("GmailQAValidUsername");

		/*obj3.AddMoreEmailAddressClick(email);
		obj3.SaveandNextClick();
		Thread.sleep(10000);*/

		obj3.ANDConditionClick();
		Thread.sleep(2000);
		obj3.ANDDropdown1Click("User Property");
		obj3.ANDDropdown2Click("email");
		obj3.ANDDropdown3Click("Exactly matches");
		obj3.ANDDropdown4Click(email);
		
		Thread.sleep(2000);
		obj3.RefreshContactsClick();
		Thread.sleep(5000);
		obj3.NumberOfContactsClick();
		
		Thread.sleep(5000);
		obj3.SaveandNextClick();
		Thread.sleep(10000);
		
		
		EmailMarketing_RecurringActive rec_obj = new EmailMarketing_RecurringActive(driver);

		rec_obj.RemoveDuplicateCheckboxClick();             // To Enable Remove Duplicate feature
		Thread.sleep(1000);
		
		rec_obj.DailyClick();

		rec_obj.Daily_StartDateClick();
		rec_obj.Daily_EndDateClick();
		rec_obj.EndDate_NextMonthClick();
		rec_obj.EndDate_SelectDateClick();
		rec_obj.Daily_TimeClick();

		set.setScheduleDateAndTime(); // Increement 1 min only

		Thread.sleep(2000);
		rec_obj.Daily_ExclusionDateClick();
		Thread.sleep(2000);
		rec_obj.Daily_ExclusionDate_SelectDateClick();
		rec_obj.Exclusion_NextMonthClick();
		rec_obj.Exclusion_SelectDateClick();
		rec_obj.Daily_ExclusionDate_SaveClick();
		Thread.sleep(2000);
		rec_obj.Daily_PreviewClick();
		rec_obj.Daily_ExcludingPreviewClick();
		rec_obj.Daily_ScheduleClick();
		Thread.sleep(2000);
		Thread.sleep(5000);

		// Assert name of campaign

		String DailyCampName = driver.findElement(obj1.daliyCampName).getText();

		System.out.println("Campaign Name = " + DailyCampName);

		SoftAssert s_assert = new SoftAssert();
		s_assert.assertEquals(DailyCampName, name);
		s_assert.assertAll();
		System.out.println("Assertion -> Name of the campaign is matched");

		String campaigndetails = driver.findElement(obj1.dailyCampDetails).getText();
		System.out.println("Start Date, End date and Time of Campaign = " + campaigndetails);

		Thread.sleep(150000); // Wait for 2.5 min

		// Open Gmail and verify subject of the email

		((JavascriptExecutor) driver).executeScript("window.open('','_blank');");
		Thread.sleep(2000);

		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());

		driver.switchTo().window(tabs.get(1)); // switches to first tab

		set.getGmailURL();

		Thread.sleep(120000); // Wait for 2 min

		driver.findElement(By.xpath(prop.getProperty("GmailSignInClick"))).click();

		GmailLoginPages page_obj = new GmailLoginPages(driver);

		page_obj.enterUsername(prop.getProperty("GmailQAValidUsername"));
		page_obj.nextButtonClick();

		Thread.sleep(2000);

		page_obj.enterPassword(prop.getProperty("GmailQAValidPassword"));
		page_obj.signInClick();

		Thread.sleep(7000);

		page_obj.openEmail();

		page_obj.showDetails();
		
		Thread.sleep(1000);
		String FROMName = page_obj.printFROMName();
		System.out.println("Name of the sender = "+FROMName);
		s_assert.assertEquals(FROMName, prop.getProperty("Email_FROMName"));
		System.out.println("Assertion of FROM Name is done");
				
		String FROMEmailAddress = page_obj.printFROMEmailAddress();
		System.out.println("EmailAddress of the sender = "+FROMEmailAddress);
		boolean val = prop.getProperty("SES_SenderID").contains(FROMEmailAddress);
				
		s_assert.assertEquals(val, true);        
		System.out.println("Assertion of FROM Email Address is done"); 
		
		String emailsubject = page_obj.printemailSubject();
		System.out.println("Subject of email = " + emailsubject);

		String emailmessage = page_obj.printemailMessage();
		System.out.println("Message content of email = " + emailmessage);
		System.out.println("---------------------------------------");

		s_assert.assertEquals(emailsubject, subject_data);
		System.out.println("Subject of the email is verified ");

		Thread.sleep(2000);
		page_obj.DeleteAllMails();
		Thread.sleep(2000);
		System.out.println("Email is deleted successfully");

		page_obj.accountClick();
		Thread.sleep(1500);
		page_obj.signout();
		

		s_assert.assertAll();
		System.out.println("Creation of recurring daily campaign is completed");
		
		// Now move to Analytics tab
		

	}

	
	  @AfterMethod public void kill() { 
	
		  System.out.println("Closing the browser now"); 
		  driver.quit();
	
	   }
	

}