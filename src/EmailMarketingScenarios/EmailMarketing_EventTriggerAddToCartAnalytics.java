package EmailMarketingScenarios;

import java.io.File;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import CartRecoveryPages.CartRecovery_Homepage;
import EmailMarketingPages.EM_Broadcast_Template;
import EmailMarketingPages.EM_Campains_Broadcast;
import EmailMarketingPages.EM_EventTriggeredCreateCampaign;
import GmailLogin.GmailLoginPages;
import common_Classes.Homepage;
import common_Classes.Loginpage;
import common_Classes.Setup_class;
/**
 * @author - Navpreet
 */
public class EmailMarketing_EventTriggerAddToCartAnalytics {
	
public static WebDriver driver;
	
	
	@Test
	public void EventTrigger_AddCartAnalyze() throws Exception{
		
		Setup_class set = new Setup_class(driver);
		
		driver = set.setup();
				
		// Code to load the Property file
		Properties prop = set.loadPropertyFile();
		
		set.getLiveURL();
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
		obj.uname(set.getWoocommercetestUsername());
		obj.upswd(set.getWoocommercetestPassword());
		
		obj.ulogin();
		
		/*// Add code to select account
		set.selectAccountNav();*/
		
		System.out.println("User has logged in successfully");
			
		// Go to EventTrigger and Check Stats
	
		EM_EventTriggeredCreateCampaign eventobj = new EM_EventTriggeredCreateCampaign(driver);
	
		// Code to get Campaign ID
		
		String filename = set.getEventAddCartFileName();
		String id = set.BufferReaderMethod(filename);

		System.out.println("Data present in file = " + id);
		
		set.getEventStatsPage(id);
		
		File scrFile1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile1, new File("./Screenshots/"+this.getClass()+System.currentTimeMillis()+".png"));
		
		System.out.println("Screenshot of Stats page is taken successfully");
		
		boolean value = eventobj.EventTrigger_PrintContactsFromStats(prop.getProperty("GmailWoocommerceOneUsername"));
		System.out.println("Boolean value for Stats = "+value);
			
		if (value == true) {
			   System.out.println("PASS , i.e. Required Gmail ID is contained in the Sent heading.");
			   }
			   else {
			   System.out.println("FAIL , i.e. Required Gmail ID is NOT contained in Sent heading");
			   }
	
	}
	

	@AfterMethod
	public void kill() throws Exception
	{
		// Inactivate event trigger
		
		Setup_class set = new Setup_class(driver);
		EM_EventTriggeredCreateCampaign eventobj = new EM_EventTriggeredCreateCampaign(driver);
		Properties prop = set.loadPropertyFile();
		driver.get(prop.getProperty("EventTrigger_CampaignsURL"));

		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(eventobj.EventTrigger_InactiveCampaign)).build().perform();	
		Thread.sleep(2000);
	
		eventobj.EventTrigger_InactiveCampaignClick();
		Thread.sleep(3000);
		
		System.out.println("Closing the browser now");
		driver.quit();
		
	}
		
		
}
