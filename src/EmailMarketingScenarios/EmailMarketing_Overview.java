package EmailMarketingScenarios;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import EmailMarketingPages.EM_Broadcast_Template;
import common_Classes.Homepage;
import common_Classes.Loginpage;
import common_Classes.Setup_class;

public class EmailMarketing_Overview {

public static WebDriver driver;
	
	
	@Test
	public void Email_Overview() throws Exception{
		
		Setup_class set = new Setup_class(driver);
		
		driver = set.setup();
				
		// Code to load the Property file
		
		Properties prop = set.loadPropertyFile();
		
	//	driver.get("https://pre-prod-102.betaout.com/");
	//	driver.get(prop.getProperty("PreProd_URL"));
		
		set.getLiveURL();
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
//		obj.uname("Woocommercetest@y0v.in");
//		obj.uname(prop.getProperty("Username_Woocommerce"));
	
		obj.uname(set.getShivangiUsername());
		
//		obj.upswd("Woocommercetest");
//		obj.upswd(prop.getProperty("Password_Woocommerce"));
	
		obj.upswd(set.getShivangiPassword());
		obj.ulogin();
		
		System.out.println("User has logged in successfully");
				
		Homepage homeobj = new Homepage(driver);
		homeobj.email_mktg_click();
	
		
		EM_Broadcast_Template obj1 = new EM_Broadcast_Template(driver);
		obj1.OverviewClick();
		obj1.PrintOverviewInfoClick();

	
	}

	
@AfterMethod
public void kill()
{
	System.out.println("Closing the browser now");
	driver.quit();
	
	
}
}