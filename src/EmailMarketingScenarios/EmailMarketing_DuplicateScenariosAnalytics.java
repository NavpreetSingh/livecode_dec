package EmailMarketingScenarios;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import EmailMarketingPages.EM_Broadcast_Template;
import EmailMarketingPages.EM_Campains_Broadcast;
import EmailMarketingPages.EM_EventTriggeredCreateCampaign;
import EmailMarketingPages.EM_Send_Schedule;
import EmailMarketingPages.EM_Tageting_Page;
import GmailLogin.GmailLoginPages;
import common_Classes.Homepage;
import common_Classes.Loginpage;
import common_Classes.Setup_class;

/**
 * @author - Navpreet
 */
public class EmailMarketing_DuplicateScenariosAnalytics {

	public static WebDriver driver;

	@Test
	public void EmailMarketing_DuplicateCampaignAnalyze() throws Exception {

		System.out.println(
				"Scenario - Create campaign with Remove Duplicate check on > clone it and now untick the Remove Duplicate check > Verify Stats ");

		Setup_class set = new Setup_class(driver);

		driver = set.setup();

		// Code to load the Property file

		Properties prop = set.loadPropertyFile();
		set.getLiveURL();

		Loginpage obj = new Loginpage(driver);

		obj.loginbutton();
		obj.uname(set.getWoocommercetestUsername());
		obj.upswd(set.getWoocommercetestPassword());

		obj.ulogin();

		/*
		 * // Add code to select account set.selectAccountNav();
		 */

		System.out.println("User has logged in successfully");

		// Add code to verify counts and stats

		SoftAssert s_assert = new SoftAssert();
		EM_EventTriggeredCreateCampaign eventobj = new EM_EventTriggeredCreateCampaign(driver);
		Homepage homeobj = new Homepage(driver);

		// Search for the campaign using SearchBox

		homeobj.search_click();
		Thread.sleep(1500);
		homeobj.Search_1stDropdown();
		homeobj.Search_ByCampaign(); // Search via Campaign name

		String filename = set.getDuplicateFileName();
		String name = set.BufferReaderMethod(filename);

		System.out.println("Data present in file = " + name);

		System.out.println(name);
		homeobj.EnterDataToSearch(name);
		homeobj.SearchSubmitButton();
		Thread.sleep(5000);

		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(eventobj.EventTrigger_StatsCampaign)).build().perform();
		Thread.sleep(2000);

		eventobj.EventTrigger_StatsCampaignClick();
		Thread.sleep(3000);

		File scrFile1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile1,new File("./Screenshots/" + this.getClass() + System.currentTimeMillis() + ".png"));

		System.out.println("Screenshot of Stats page is taken successfully");

		boolean value = eventobj.EventTrigger_PrintContactsFromStats(prop.getProperty("GmailWoocommerceOneUsername"));
		System.out.println("Boolean value for Stats = " + value);

		if (value == true) {
			System.out.println("PASS , i.e. Required Gmail ID is contained in the Sent heading.");
		} else {
			System.out.println("FAIL , i.e. Required Gmail ID is NOT contained in Sent heading");
		}

		driver.navigate().back();

		driver.navigate().refresh();

		s_assert.assertAll();

	}

	@AfterMethod
	public void kill() {
		System.out.println("Closing the browser now");
		driver.quit();

	}

}
