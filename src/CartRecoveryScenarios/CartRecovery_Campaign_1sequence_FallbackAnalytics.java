package CartRecoveryScenarios;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import CartRecoveryPages.CartRecovery_CampaignConditions;
import CartRecoveryPages.CartRecovery_CampaignName;
import CartRecoveryPages.CartRecovery_CartRecoveryCampaign;
import CartRecoveryPages.CartRecovery_Design;
import CartRecoveryPages.CartRecovery_Homepage;
import CartRecoveryPages.CartRecovery_SelectTemplate;
import GmailLogin.GmailLoginPages;
import SMSMarketingPages.SMSMarketing_Broadcast_Active;
import SMSMarketingPages.SMSMarketing_CampaignType;
import SMSMarketingPages.SMSMarketing_Design;
import SMSMarketingPages.SMSMarketing_Homepage;
import SMSMarketingPages.SMSMarketing_Target;
import common_Classes.*;
import common_Classes.Account_Settings;
import common_Classes.Loginpage;
import promocode_Pages.Onsite_CreateCampaign;
import common_Classes.Onsite_Design;
import common_Classes.Homepage;
import promocode_Pages.Onsite_Select_Template;
import common_Classes.Onsite_Target_Page;
/**
 * @author - Navpreet
 */

public class CartRecovery_Campaign_1sequence_FallbackAnalytics{


	public static WebDriver driver;

	@Test
	public void CartRecovery_1sequence() throws Exception
	
	{
		
		System.out.println("Scenario : Cart Recovery > Campaign > Create Campaign with Dynamic Property and Fallback Property in Subject > Check Mail");
			
		Setup_class set = new Setup_class(driver);
		
		driver = set.setup();
	
		set.getLiveURL();
		SoftAssert s_assert = new SoftAssert();
		
		Properties prop = set.loadPropertyFile();
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
		obj.uname(set.getWoocommercetestUsername());
		obj.upswd(set.getWoocommercetestPassword());
		obj.ulogin();
		
		/*// Add code to select account
		set.selectAccountNav();*/
		
		System.out.println("User has logged in successfully");
		
		set.getQACartCampaignPage();
		
		// Code to print Stats of campaign
		
		
		String filename = set.getCart1SeqFileName();
		String campID = set.BufferReaderMethod(filename);
		
		System.out.println("Data present in file = " + campID);
		
		Thread.sleep(2000);
		
		
		String SentData = driver.findElement(By.xpath("//*[@id = '"+campID+"_totalSent']")).getText();
		System.out.println("Data printed for Sent = " +SentData);
		s_assert.assertEquals(SentData, "1");
		
		
		String OpenData = driver.findElement(By.xpath("//*[@id = '"+campID+"_totalOpened']")).getText();
		System.out.println("Data printed for Open = " +OpenData);
		s_assert.assertEquals(OpenData, "1");
		
		
		String OpenPercentData = driver.findElement(By.xpath("//*[@id = '"+campID+"_openPercentage']")).getText();
		System.out.println("Data printed for OpenPercent = " +OpenPercentData);
		s_assert.assertEquals(OpenPercentData, "100%");
		
		
		String ClickedData = driver.findElement(By.xpath("//*[@id = '"+campID+"_totalClicked']")).getText();
		System.out.println("Data printed for Clicked = " +ClickedData);
		
		String ClickedPercentData = driver.findElement(By.xpath("//*[@id = '"+campID+"_clickPercentage']")).getText();
		System.out.println("Data printed for ClickedPercent = " +ClickedPercentData);
		
		driver.findElement(By.xpath("//*[@id = 'status_"+campID+"']")).click();      // Inactivate campaign
		Thread.sleep(2000);
		
		s_assert.assertAll();
		System.out.println("Cart recovery scenario is inactivated successfully");

		
	}
	
	@AfterMethod
	public void kill()
	{	
		System.out.println("Closing the browser now");
		driver.quit();
		
	}
	
}
